import { View, Text } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import TabNaviagtion from './TabNaviagtion'
import Products from '../Home/Products'

const Stacks = () => {

  const Stack = createNativeStackNavigator()

  return (
    <>
    
    <NavigationContainer> 
      <Stack.Navigator screenOptions={{headerShown:false}} >
        <Stack.Screen name="home" component={TabNaviagtion}/>
        <Stack.Screen name="product" component={Products} />
      </Stack.Navigator>    
    </NavigationContainer>

    
    </>
   
  )
}

export default Stacks
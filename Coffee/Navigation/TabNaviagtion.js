import { View, Text } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Home from '../Home/Home'
import AntDesign from 'react-native-vector-icons/AntDesign'

const TabNaviagtion = () => {

  const Tab = createBottomTabNavigator()

  return (
    <>

      <Tab.Navigator 
      screenOptions={{
        headerShown: false,
        tabBarShowLabel:false,
        tabBarIcon:()=><AntDesign name="home" size={35} color="white" />,
        tabBarStyle :{
          backgroundColor:"#C34D4D",
          borderRadius:40
        }
        }} >
        <Tab.Screen name="homes" component={Home} />
      </Tab.Navigator>
    </>
  )
}

export default TabNaviagtion
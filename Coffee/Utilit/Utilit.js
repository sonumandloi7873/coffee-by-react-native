export const coffeeCategories = [
  {
    id : 1,
    title : "Cappuccino",
  },
  {
    id : 2,
    title : "Latte",
  },
  {
    id : 3,
    title : "Espresso",
  },
  {
    id : 4,
    title : "Mocha",
  },
  {
    id : 5,
    title : "Americano",
  }
]
  export const CoffeeData = [
      {
        "id": "C1",
        "name": "Americano",
        "description": "The americano is often mistaken for just a standard black coffee, but it's so much more than that",
        "roasted": "Medium Roasted",
        "imageURL": "https://majestycoffee.com/cdn/shop/articles/americano_b74a8154-454b-4f74-9a6c-95fbc4152ed3.jpg?v=1684048195",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.38",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.15",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.29",
            "currency": "$"
          }
        ],
        "average_rating": 4.7,
        "ratings_count": 6879,
        "favourite": false,
        "type": "coffee",
        "index": 0
      },
      {
        "id": "C2",
        "name": "Cortado",
        "description": "The americano is often mistaken for just a standard black coffee, but it's so much more than that.",
        "roasted": "Medium Roasted",
        "imageURL": "https://recipes.net/wp-content/uploads/2024/01/how-to-drink-cortado-1705761104.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.35",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.10",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.25",
            "currency": "$"
          }
        ],
        "average_rating": 4.5,
        "ratings_count": 6529,
        "favourite": false,
        "type": "coffee",
        "index": 1
      },
      {
        "id": "C3",
        "name": "Mocha",
        "description": "Deliciously sweet, nutty and chocolatey. Find out what is a mocha, where it came from & how it's different.",
        "roasted": "Medium Roasted",
        "imageURL": "https://www.thedailymeal.com/img/gallery/the-classic-mocha-is-a-coffee-shop-staple-but-what-is-it/intro-1686048648.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.45",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.20",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.45",
            "currency": "$"
          }
        ],
        "average_rating": 4.6,
        "ratings_count": 6629,
        "favourite": false,
        "type": "coffee",
        "index": 2
      },
      {
        "id": "C4",
        "name": "Macchiato",
        "description": "An espresso coffee drink, topped with a small amount of foamed milk to enable the espresso taste to shine through.",
        "roasted": "Medium Roasted",
        "imageURL": "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Caff%C3%A8_Espresso_Macchiato_Schiumato.jpg/640px-Caff%C3%A8_Espresso_Macchiato_Schiumato.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.30",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.05",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.30",
            "currency": "$"
          }
        ],
        "average_rating": 4.6,
        "ratings_count": 5629,
        "favourite": false,
        "type": "coffee",
        "index": 3
      },
      {
        "id": "C5",
        "name": " Flat White",
        "description": "A newcomer in the coffee world that is increasing popular. Learn exactly what a flat white is and where it came from, here.",
        "roasted": "Medium Roasted",
        "imageURL": "https://images.immediate.co.uk/production/volatile/sites/2/2021/11/Flat-White-d195a5f.png?quality=90&resize=556,505",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.50",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.10",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.40",
            "currency": "$"
          }
        ],
        "average_rating": 4.4,
        "ratings_count": 5929,
        "favourite": false,
        "type": "coffee",
        "index": 4
      },
      {
        "id": "C6",
        "name": " Decaf",
        "description": "Many people like to enjoy a cup of decaf coffee as it offers the delicious taste with less caffeine content. But what is decaf coffee, exactly?",
        "roasted": "Medium Roasted",
        "imageURL": "https://japanesecoffeeco.com/cdn/shop/articles/Everything_you_need_to_know_about_decaf_coffee.jpg?v=1638514649",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.60",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.20",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.50",
            "currency": "$"
          }
        ],
        "average_rating": 4.2,
        "ratings_count": 6929,
        "favourite": false,
        "type": "coffee",
        "index": 5
      },
      {
        "id": "C7",
        "name": "Irish",
        "description": "You will recognise an Irish coffee as it is served in a distinctive toddy glass, with a generous layer of cream. But exactly what is it?",
        "roasted": "Medium Roasted",
        "imageURL": "https://www.blogtasticfood.com/wp-content/uploads/2020/03/Baileys-Irish-Coffee-Foodgawker-1-of-1.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.30",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.00",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.30",
            "currency": "$"
          }
        ],
        "average_rating": 4.4,
        "ratings_count": 6729,
        "favourite": false,
        "type": "coffee",
        "index": 6
      },
      {
        "id": "C8",
        "name": "Iced",
        "description": "On a hot summer's day, is there anything better than sipping on an iced coffee? Exactly what is an iced coffee & how do you make them?",
        "roasted": "Medium Roasted",
        "imageURL": "https://i.insider.com/60f1fb537b0ec5001800a9bd?width=1200&format=jpeg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.40",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.10",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.40",
            "currency": "$"
          }
        ],
        "average_rating": 4.3,
        "ratings_count": 6229,
        "favourite": false,
        "type": "coffee",
        "index": 7
      },
      {
        "id": "C9",
        "name": "Cold Brew",
        "description": "Cold brew coffee has taken the world by storm in recent years, but what is it and how does it differ from an iced coffee?",
        "roasted": "Medium Roasted",
        "imageURL": "https://ineffablecoffee.com/wp-content/uploads/2021/05/blog-ineffablecoffee-roasters-cafe-cold-brew-00.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.40",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.20",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.40",
            "currency": "$"
          }
        ],
        "average_rating": 4.5,
        "ratings_count": 6429,
        "favourite": false,
        "type": "coffee",
        "index": 8
      },
      {
        "id": "C10",
        "name": "Café au Lait",
        "description": "The Café au Lait comes from France and means 'coffee with milk', but what exactly is Café au Lait coffee and how does it differ to other types?",
        "roasted": "Medium Roasted",
        "imageURL": "https://cdn.shopify.com/s/files/1/0069/6467/4613/files/shutterstock_317967329_2048x2048.jpg?v=1634343990",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.44",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.15",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.50",
            "currency": "$"
          }
        ],
        "average_rating": 4.6,
        "ratings_count": 6329,
        "favourite": false,
        "type": "coffee",
        "index": 9
      },
      {
        "id": "C11",
        "name": "Drip",
        "description": "Drip coffee is one of the most popular ways to prepare and enjoy coffee, but what is it exactly and where did it come from?",
        "roasted": "Medium Roasted",
        "imageURL": "https://www.nescafe.com/gb/sites/default/files/2023-08/what-is-drip-coffee-desktop.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.45",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.20",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.55",
            "currency": "$"
          }
        ],
        "average_rating": 4.5,
        "ratings_count": 6359,
        "favourite": false,
        "type": "coffee",
        "index": 10
      },
      {
        "id": "C12",
        "name": "Instant",
        "description": "Instant coffee is a staple in our lives. It’s quick and easy to prepare and, not to mention, delicious. But what is instant coffee? ",
        "roasted": "Medium Roasted",
        "imageURL": "https://www.foodrepublic.com/img/gallery/what-instant-coffee-is-actually-made-of/intro-1695738252.jpg",
        "ingredients": "Milk",
        "special_ingredient": "With Steamed Milk",
        "prices": [
          {
            "size": "S",
            "price": "1.85",
            "currency": "$"
          },
          {
            "size": "M",
            "price": "3.50",
            "currency": "$"
          },
          {
            "size": "L",
            "price": "4.65",
            "currency": "$"
          }
        ],
        "average_rating": 4.5,
        "ratings_count": 6859,
        "favourite": false,
        "type": "coffee",
        "index": 11
      }
    ]
  
    // console.log("coffeedata",coffeeData)

  
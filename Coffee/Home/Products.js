import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import Evillcons from 'react-native-vector-icons/EvilIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'

const Products = (props) => {

    const item = props.route.params
    const navigation = useNavigation()
  const[activeCategories,setActiveCategories]=useState(item.prices[0])
//   console.log("activeCategories",activeCategories)

    return (
        <View style={styles.container}>
            <Image source={{
                uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtZscw55oP3askvNAyQEOU-p3d8sUWNOD29g&usqp=CAU"
            }}
                style={{ width: "full", height: 210, borderBottomLeftRadius: 50, borderBottomRightRadius: 50 }}
            />
            <SafeAreaView style={styles.containerBody} >
                <View style={styles.headerLike}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Evillcons name="arrow-left" size={45} color="white" />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ borderRadius: 50, borderWidth: 2, borderColor: "white", padding: 8 }}>
                        <AntDesign name="heart" size={25} color="white" />
                    </TouchableOpacity>
                </View>

                <View style={styles.card} >
                    <Image source={{ uri: item?.imageURL }} style={{ height: 180, width: 180, borderRadius: 90 }} />
                </View>

                <View style={{ backgroundColor: "#EA9755", flexDirection: "row", alignItems: "center", borderRadius: 20, padding: 5, paddingHorizontal: 5, width: 70, justifyContent: "space-around", marginTop: 10, marginLeft: 10 }}>
                    <AntDesign name='star' size={20} color='white' />
                    <Text style={styles.rating}>{item?.average_rating}</Text>
                </View>

                <View style={styles.name}>
                    <Text style={{ fontSize: 30, fontWeight: "bold" }}>{item.name}</Text>
                    <Text style={{ fontSize: 25, fontWeight: "bold" }}>$ {activeCategories.price}</Text>
                </View>
                <View style={{ marginHorizontal: 1,marginLeft:10 }} >
                    <Text style={{ fontSize: 30, fontWeight: "bold" }}>Coffee</Text>
                    <View style={{ flexDirection: "row", justifyContent: "space-between",marginTop:20 }} >
                        <TouchableOpacity
                            onPress={() => setActiveCategories(item.prices[0])}
                            style={[styles.coffeeCategoriestouch, { backgroundColor: activeCategories.size == item.prices[0].size ? "brown" : '#f2e6d9' }]}
                        >
                            <Text style={[styles.coffeeCategoriesTitle, { color: activeCategories.size === item.prices[0].size ? "white" : 'black' }]}>Small</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setActiveCategories(item.prices[1])}
                            style={[styles.coffeeCategoriestouch, { backgroundColor: activeCategories.size == item.prices[1].size ? "brown" : '#f2e6d9' }]}
                        >
                            <Text style={[styles.coffeeCategoriesTitle, { color: activeCategories.size === item.prices[1].size ? "white" : 'black' }]}>Medium</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setActiveCategories(item.prices[2])}
                            style={[styles.coffeeCategoriestouch, { backgroundColor: activeCategories.size == item.prices[2].size ? "brown" : '#f2e6d9' }]}
                        >
                            <Text style={[styles.coffeeCategoriesTitle, { color: activeCategories.size ===item.prices[2].size? "white" : 'black' }]}>Large</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
                <View style={{marginTop:20,marginLeft:10}}>
                    <Text style={{fontSize:30,fontWeight:"bold"}}>Description</Text>
                    <Text style={{fontSize:18,marginTop:10}}>{item.description}</Text>
                </View>

            </SafeAreaView>
        </View>
    )
}

export default Products

const styles = StyleSheet.create({
    container: {
        position: "relative",
        flex: 1,
        backgroundColor: "white"
    },
    containerBody: {
        position: "absolute",
        marginTop: 20,
    },
    headerLike: {
        marginHorizontal: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: 390,
    },
    card: {
        shadowColor: "black",
        shadowRadius: 30,
        shadowOffset: { width: 0, height: 40 },
        shadowOpacity: 0.8,
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 20,
        elevation: 50
    },
    rating: {
        color: "white",
        fontFamily: "bold",
        fontSize: 20
    },
    name: {
        marginHorizontal: 4,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 20,
        padding: 5
    },
    coffeeCategoriestouch: {
        padding: 10,
        paddingHorizontal: 20,
        borderRadius: 50,
        marginRight: 20,
    },
    coffeeCategoriesTitle: {
        fontWeight: "700",
        letterSpacing:1
    }

})
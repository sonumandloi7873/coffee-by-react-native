import { View, Text, Image, StyleSheet, SafeAreaView, TextInput, FlatList, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { CoffeeData, coffeeCategories } from '../Utilit/Utilit'
// import  Carousel  from "react-native-snap-carousel"
import CoffeeCard from './CoffeeCard'
import Carousel from 'react-native-snap-carousel'

const Home = () => {

  const[categories,setCategories]=useState("")
  const[activeCategories,setActiveCategories]=useState(1)


  return (
    <>
      <View style={styles.container}>
        <Image source={{
          uri: "https://media.istockphoto.com/id/1253450580/vector/vector-seamless-pattern-with-coffee-beans.jpg?s=612x612&w=0&k=20&c=yUGY7MgkV4BD41QML4LmDJyJmcs7y6_C4AZt2sjj4cM="
        }}
          style={{ width: "full", height: 210, opacity: 0.3 }}
        />
        <SafeAreaView style={styles.safeContainer}>
          <View style={styles.header} >
            <FontAwesome5 name="user-circle" size={35} color="brown" />
            <View style={styles.map} >
              <Feather name="map-pin" size={25} color="brown" />
              <Text style={{ color: "black", fontSize: 20, fontWeight: "bold", marginLeft: 10, marginBottom: 5 }} >New York,NYC</Text>
            </View>
            <Feather name="bell" size={30} color="brown" />
          </View>

          {/* search */}

          <View style={styles.search} >
            <TextInput placeholder='Search' style={{ width: 320, paddingHorizontal: 25, fontSize: 22 }} />
            <Feather name="search" size={30} color="white" style={{ borderRadius: 50, padding: 10, backgroundColor: "#b30000", opacity: 0.7, color: "white" }} />

          </View>

          {/* categories */}
          <View style={styles.coffeeCategories}>
            <FlatList 
            horizontal
            showsHorizontalScrollIndicator={false}
            data={coffeeCategories}
            keyExtractor={item=>item.id}
            renderItem={({item})=>{
              return(
                <TouchableOpacity
                onPress={()=>setActiveCategories(item.id)}
                style={[styles.coffeeCategoriestouch, {backgroundColor: activeCategories == item.id ? "brown" :'#f2e6d9'}]}
                >
                  <Text style={[styles.coffeeCategoriesTitle,{ color:activeCategories === item.id ? "white" : 'black'}]}>{item.title}</Text>
                </TouchableOpacity>
              )
            }}
            />
          </View>

          {/* cards */}
          <View style={{marginTop:20}}>
            <Carousel 
            data={CoffeeData}
            renderItem={({item})=><CoffeeCard item={item} />}
            firstitem={1}
            inactiveSliderOpacity={0.75}
            inactiveSiderScale={0.77}
            sliderWidth={400}
            itemWidth={260}
            sliderStyle={{display:"flex",alignItems:"center"}}
            />
          </View>

        </SafeAreaView>

      </View>
    </>
  )
}

export default Home

const styles = StyleSheet.create({
  container: {
    position: "relative",
    flex: 1,
    backgroundColor: "white"
  },
  safeContainer: {
    flex: 1,
    position: "absolute",
    top: 10,
    padding: 7,
  },
  header: {
    flex: 1,
    width: 390,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20
  },
  map: {
    flexDirection: "row",
  },
  search: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 50,
    padding: 5,
    backgroundColor: "#f2e6d9",
    marginTop: 20
  },
  coffeeCategories:{
    paddingHorizontal:5,
    marginTop:20,
    // width:"full",
  },
  coffeeCategoriestouch:{
    padding:10,
    paddingHorizontal:20,
    borderRadius:50,
    marginRight:20,
  },
  coffeeCategoriesTitle:{
    fontWeight:"500"
  }

})
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useNavigation } from '@react-navigation/native'

const CoffeeCard = ({ item }) => {

    const navigation = useNavigation()

    return (
        <View style={styles.CoffeeCard} >
            <View style={styles.card} >
                <Image source={{ uri: item?.imageURL }} style={{ height: 150, width: 150, borderRadius: 70 }} />
            </View>
            <View style={styles.cardtext}>
                <Text style={styles.cardname}>{item?.name}</Text>
                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", flexDirection: "row", alignItems: "center", borderRadius: 20, padding: 3, paddingHorizontal: 5, width: 70, justifyContent: "space-around", marginTop: 10 }}>
                    <AntDesign name='star' size={20} color='white' />
                    <Text style={styles.rating}>{item?.average_rating}</Text>
                </View>
                <View style={{ flexDirection: "row",justifyContent:"space-between",alignItems:"flex-end" }}>
                    <View>
                        {
                            item?.prices.map((data) => {
                                return (
                                    <>
                                    <View key={data?.price}>
                                        <View style={styles.cardSize}>
                                            <Text style={{ color: "white", fontSize: 18 }} >Size :-</Text>
                                            <Text style={{ color: "white", fontSize: 18 }}>{data.size}</Text>
                                        </View>
                                        <View style={styles.cardPrice}>
                                            <Text style={{ color: "white", fontSize: 18 }}>Price :-</Text>
                                            <View style={{ flexDirection: "row" }}>
                                                <Text style={{ color: "white", fontSize: 18 }}>{data?.currency}</Text>
                                                <Text style={{ color: "white", fontSize: 18 }}>{data?.price}</Text>
                                            </View>
                                        </View>
                                        </View>
                                    </>
                                )
                            })
                        }
                    </View>
                    <TouchableOpacity onPress={()=>navigation.navigate('product',{...item})} style={{padding:10,backgroundColor:"white",borderRadius:50,fontWeight:"bold"}}>
                        <AntDesign name="plus" size={25} color="brown" />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default CoffeeCard

const styles = StyleSheet.create({
    CoffeeCard: {
        borderRadius: 40,
        backgroundColor: "brown",
        height: 370,
        width: 250,
        marginTop: 35,
    },
    card: {
        shadowColor: "black",
        shadowRadius: 30,
        shadowOffset: { width: 0, height: 40 },
        shadowOpacity: 0.8,
        flexDirection: "row",
        justifyContent: "center",
        marginTop: -35,
        elevation:20

    },
    cardtext: {
        paddingHorizontal: 20,
        marginTop: 5,
    },
    cardname: {
        fontSize: 30,
        color: "white",
        fontWeight: 'bold',
        zIndex: 10
    },
    rating: {
        color: "white",
        fontFamily: "bold",
        fontSize: 20
    },
    cardSize: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: 65
    },
    cardPrice: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: 105
    }
})
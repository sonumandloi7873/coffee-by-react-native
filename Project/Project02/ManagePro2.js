import { StatusBar, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import LiceScorePro2 from './SelectMatchPro2'
import ContiueOngoingGame from './ContiueOngoingGame'
import CreateNewFixture from './CreateNewFixture'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'

const ManagePro2 = () => {

  const Tab = createMaterialTopTabNavigator()

  return (
    <>
      <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingHorizontal: 20, height: 80, backgroundColor: "#39BF90" }}>
        <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
          <MaterialIcons name="arrow-back-ios-new" size={20} color="white" />
          <Text style={{ marginLeft: 20, color: "white" }}>Live Scoring</Text>
        </View>
        <View style={{ position: "relative" }}>
          <EvilIcons name="bell" size={30} color="white" />
          <Text style={{ position: "absolute", width: 10, height: 10, borderWidth: 2, borderColor: "white", borderRadius: 5, right: 4, top: 5, backgroundColor: "#39BF90" }}></Text>
        </View>
      </View>


      <Tab.Navigator screenOptions={{
        tabBarShowLabel: false,
        tabBarStyle: { backgroundColor: "#39BF90" }
      }} >
        <Tab.Screen name="Select Match" component={LiceScorePro2}
          options={{
            tabBarIcon: ({ focused }) => {
              return (
                <View style={[styles.activeitem, styles.activeitem1]}>
                  <Text style={focused ? styles.activeText : styles.inactiveText}>Select Match</Text>
                </View>
              )
            },
          }} />
        <Tab.Screen name="Contiue ongoing Game" component={ContiueOngoingGame}
          options={{
            tabBarIcon: ({ focused }) => {
              return (
                <View style={[styles.activeitem, styles.activeitem2]}>
                  <Text style={focused ? styles.activeText : styles.inactiveText}>Contiue Ongoing Game</Text>
                </View>
              )
            },
          }} />
        <Tab.Screen name="Create New Fixture" component={CreateNewFixture}
          options={{
            tabBarIcon: ({ focused }) => {
              return (
                <View style={[styles.activeitem, styles.activeitem3]}>
                  <Text style={focused ? styles.activeText : styles.inactiveText}>Create New Fixture</Text>
                </View>
              )
            },
          }} />
      </Tab.Navigator>

    </>
  )
}

export default ManagePro2

const styles = StyleSheet.create({
  activeitem: {
    // borderWidth: 1,
    marginLeft: -50,
    height: 25,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  activeText: {
    fontSize: 13,
    color: "white",
    borderBottomWidth: 1,
    borderColor: "white"

  },
  inactiveText: {
    fontSize: 13,
    color: "white",
  },
  activeitem1: {
    width: 115
  },
  activeitem2: {
    width: 150,
    marginLeft: -70

  },
  activeitem3: {
    width: 120

  },
})
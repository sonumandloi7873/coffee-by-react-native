import { Image, StyleSheet, Text, TextInput, View } from 'react-native'
import React from 'react'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

const LiceScorePro2 = () => {
  return (
    <View style={{ marginHorizontal: 10, marginTop: 20 }}>
      <View style={{ borderWidth: 2, borderRadius: 5, borderColor:"grey", flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
        <TextInput placeholder='Select Series' style={{ width: 340, paddingHorizontal: 10,}} />
        <MaterialIcons name="arrow-drop-down" size={30} />
      </View>

      <LeagueMatch time={"14/05/2024 - 4:00pm"} />
      <LeagueMatch time={"16/05/2024 - 4:00pm"}  />
      <LeagueMatch time={"18/05/2024 - 4:00pm"}  />

    </View>
  )
}


function LeagueMatch({time}) {
  return (

    <View style={{ width: 390, height: 150, marginTop: 20, borderRadius: 10, backgroundColor: "white", elevation: 20 }}>
      <View style={{ height: 30, backgroundColor: "#39BF90", paddingHorizontal: 8, flexDirection: "row", justifyContent: "space-between", alignItems: "center", borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
        <Text style={{ color: "white" }}>League Name</Text>
        <Text style={{ color: "white" }}>{time}</Text>
      </View>
      <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", height: 120 }}>
        <Image source={{
          uri: "https://upload.wikimedia.org/wikipedia/commons/4/4b/Jfcc-nw.jpg"
        }}
          style={{ width: 45, height: 45, borderRadius: 50 }}
        />
        <View style={{ justifyContent: "center", alignItems: "center", marginHorizontal: 10 }}>
          <Text style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>JFCC vs ABCD</Text>
          <Text>Information will come here</Text>
          <View style={{ alignItems: "center", justifyContent: "center", marginTop: 10, borderRadius: 20, padding: 5, paddingHorizontal: 10, backgroundColor: "#D2F8CD" }}>
            <Text style={{ color: "black" }}>Start Scoring</Text>
          </View>
        </View>
        <Image source={{
          uri: "https://dnd3y8e5nonx2.cloudfront.net/groups/avatars/73826/1380075325/display.png"
        }}
          style={{ width: 40, height: 40, borderWidth: 1, borderRadius: 50 }} />
      </View>
    </View>
  )
}

export default LiceScorePro2

const styles = StyleSheet.create({})
import React from 'react'
import { StatusBar, StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import HomePro2 from './HomePro2'
import ManagePro2 from './ManagePro2'
import LivePro2 from './LivePro2'
import StatsPro2 from './StatsPro2'
import ProfilePro2 from './ProfilePro2'
import Foundation from 'react-native-vector-icons/Foundation'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Feather from 'react-native-vector-icons/Feather'
import FontAwesome from 'react-native-vector-icons/FontAwesome'



const TabNaviagtion = () => {

  const Tab = createBottomTabNavigator()

  return (
    <>
      <StatusBar backgroundColor="#39BF90" />
      <NavigationContainer>
        <Tab.Navigator screenOptions={{
          headerShown: false,
          tabBarStyle: { height: 55, position: "relative" },
          tabBarShowLabel: false,

        }}>
          <Tab.Screen name="Manage" component={ManagePro2}
            options={{
              tabBarIcon: ({ focused }) => {
                return (
                  <View style={focused ? styles.activeItem : styles.inActiveItem}>
                    <Ionicons name="settings-sharp" size={20} style={focused ? styles.activeIcon : styles.inActiveIcon} />
                    <Text style={focused ? styles.activeText : styles.inActiveText}>Manage</Text>
                  </View>
                )
              },
            }}
          />
          <Tab.Screen name="Home" component={HomePro2}
            options={{
              tabBarIcon: ({ focused }) => {
                return (
                  <View style={focused ? styles.activeItem : styles.inActiveItem}>
                    <Foundation name="home" size={20} style={focused ? styles.activeIcon : styles.inActiveIcon} />
                    <Text style={focused ? styles.activeText : styles.inActiveText}>Home</Text>
                  </View>
                )
              },
            }}
          />

          <Tab.Screen name="Live" component={LivePro2}
            options={{
              tabBarIcon: ({ focused }) => {
                return (
                  <View style={styles.activeItemLive}>
                    <FontAwesome name="play" size={20} style={styles.activeIconLive} />
                    <Text style={styles.activeTextLive}>Live</Text>
                  </View>
                )
              },
            }}
          />
          <Tab.Screen name="State" component={StatsPro2}
            options={{
              tabBarIcon: ({ focused }) => {
                return (
                  <View style={focused ? styles.activeItem : styles.inActiveItem}>
                    <Foundation name="graph-trend" size={20} style={focused ? styles.activeIcon : styles.inActiveIcon} />
                    <Text style={focused ? styles.activeText : styles.inActiveText}>State</Text>
                  </View>
                )
              },
            }}
          />
          <Tab.Screen name="Profile" component={ProfilePro2}
            options={{
              tabBarIcon: ({ focused }) => {
                return (
                  <View style={focused ? styles.activeItem : styles.inActiveItem}>
                    <FontAwesome name="user-circle" size={20} style={focused ? styles.activeIcon : styles.inActiveIcon} />
                    <Text style={focused ? styles.activeText : styles.inActiveText}>Profile</Text>
                  </View>
                )
              },
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </>


  )
}
export default TabNaviagtion


const styles = StyleSheet.create({

  activeItem: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,

  },
  inActiveItem: {
    justifyContent: "center",
    alignItems: "center",
  },
  activeIcon: {
    color: "black",
    fontWeight: "bold",
    fontSize: 25
  },
  inActiveIcon: {
    color: "grey"
  },
  activeText: {
    color: "black",
    fontWeight: "bold"
  },
  inActiveText: {
    color: "grey"

  },
  activeItemLive: {
    // borderWidth: 1
    position: "absolute",
    borderRadius: 50,
    padding: 12,
    paddingHorizontal: 18,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#39BF90",
    top:-30
    // width:50

  },
  activeIconLive: {
    color: "white",
  },
  activeTextLive: {
    color: "white",
    fontWeight: "600"
  }
})